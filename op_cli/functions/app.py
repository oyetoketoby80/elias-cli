import sys
from pathlib import Path
from autodiscover import AutoDiscover

class CLI():
    def __init__(self):
        self.func_map = {}

    def register(self, name=None):
        def func_wrapper(func):
            _name = name if name else func.__name__
            self.func_map[_name] = func
            return func

        return func_wrapper

    def call_command(self):
        command = sys.argv[1]
        command = self.call_registered(command)
        return command

    def run_command(self, command):
        try:
            res = eval(command, self.func_map)
        except NameError as e:
            print("No such command is found: {} ".format(str(e)))
            return
        except SyntaxError as e:
            print("Command SyntaxError: {} ".format(str(e)))
            return

        return res

    def get_command(self, name=None):
        func = self.func_map.get(name, None)
        if func is None:
            print("No such command is found: {} ".format(str(e)))
            
        return func


app = CLI()



path = Path('functions')
autodiscover = AutoDiscover(path)

autodiscover()