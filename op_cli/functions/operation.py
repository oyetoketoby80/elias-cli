from op_cli.functions.app import app
import requests

@app.register()
def pretify(json):
    return json.keys()

@app.register()
def jobs(q):
    url = f"https://indreed.herokuapp.com/api/jobs/?q={q}"
    req = requests.get(url)
    if req.status_code == 200:
        return req.json()

@app.register()
def rand(min=100, max=1000):
    """
    rand()
    """
    url = f"https://jvnyl60l6b.execute-api.eu-west-2.amazonaws.com/prod/add-number-generator?min={min}&max={max}"
    req = requests.get(url)
    return int(req.content.decode("utf-8"))